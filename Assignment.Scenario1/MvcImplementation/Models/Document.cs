﻿namespace MvcImplementation.Models
{
    public class Document
    {
        public string DocumentType { get; set; }
        public string PrintedOutput { get; set; }
        public int NumberOfCopies { get; set; }
        public int Delay { get; set; }
    }
}