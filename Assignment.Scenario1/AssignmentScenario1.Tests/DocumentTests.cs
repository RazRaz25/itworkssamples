﻿using System;
using System.IO;
using Assignment.Scenario1.Models;
using NUnit.Framework;
using NUnit.Framework.Compatibility;

namespace AssignmentScenario1.Tests
{
    [TestFixture]
    public class DocumentTests
    {
        [Test]
        public void PrintQuickTest()
        {
            var stream = new MemoryStream();
            var doc = new QuickDocument(){Name="QuickDocument -- ",Content = "This is quick content"};
            doc.Print(stream);
            stream.Seek(0,SeekOrigin.Begin);
            var document = new StreamReader(stream).ReadToEnd();
            Assert.AreEqual("QuickDocument -- This is quick content",document);
        }

        [Test]
        public void PrintStandardTest()
        {
            var stream = new MemoryStream();
            var doc = new StandardDocument() { Name = "StandardDocument -- ", Content = "This is standard content",NumberOfCopies = 2};
            doc.Print(stream);
            stream.Seek(0, SeekOrigin.Begin);
            var document = new StreamReader(stream).ReadToEnd();
            Assert.AreEqual("StandardDocument -- This is standard contentStandardDocument -- This is standard content", document);
        }
        
        [Test]
        public void PrintSlowTest()
        {
            var stream = new MemoryStream();
            var doc = new SlowDocument() { Name = "SlowDocument -- ", Content = "This is slow content", NumberOfCopies = 2,Delay = 5};
            var stopWatch = new Stopwatch();
            
            stopWatch.Start();
            doc.Print(stream);
            stopWatch.Stop();

            stream.Seek(0, SeekOrigin.Begin);
            var document = new StreamReader(stream).ReadToEnd();
            Assert.AreEqual("SlowDocument -- This is slow contentSlowDocument -- This is slow content", document);
            Assert.GreaterOrEqual(stopWatch.ElapsedMilliseconds,5000);
            Assert.Less(stopWatch.ElapsedMilliseconds,10000);
            Console.WriteLine(stopWatch.ElapsedMilliseconds);
        }
    }
}
