﻿using System.IO;
using System.Web.Mvc;
using Assignment.Scenario1.Interfaces;
using Assignment.Scenario1.Models;
using MvcImplementation.Models;

namespace MvcImplementation.Controllers
{
    public class PrintAssignmentController : Controller
    {
        public ActionResult Index(Document doc)
        {
            var returnDoc = new Document();
            if (doc == null)
            {
                return View(returnDoc);
            }
            switch (doc.DocumentType)
            {
                case "Quick":
                    returnDoc = PrintQuickDocument();
                    break;
                case "Standard":
                    returnDoc = PrintStandardDocument(doc);
                    break;
                case "Slow":
                    returnDoc = PrintSlowDocument(doc);
                    break;
            }
            return View(returnDoc);
        }

        private Document PrintQuickDocument()
        {
            return PrintDocument(
                new QuickDocument
                {
                    Content = "I am quick content", 
                    Name = "Quick Document"
                });
        }

        private Document PrintStandardDocument(Document document)
        {
            return PrintDocument(
                new StandardDocument
                {
                    Content = "I am standard content",
                    Name = "Standard Document",
                    NumberOfCopies = document.NumberOfCopies
                });
        }

        private Document PrintSlowDocument(Document document)
        {
            return PrintDocument(
                new SlowDocument
                {
                    Content = "I am slow content",
                    Name = "Slow Document",
                    NumberOfCopies = document.NumberOfCopies,
                    Delay = document.Delay
                });
        }

        private static Document PrintDocument(IPrintableDocument doc)
        {
            var stream = new MemoryStream();
            doc.Print(stream);
            stream.Seek(0, SeekOrigin.Begin);
            return new Document() {PrintedOutput = new StreamReader(stream).ReadToEnd()};
        }
    }
}