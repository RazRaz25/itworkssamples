﻿using System.IO;
namespace Assignment.Scenario1.Models
{
    public class StandardDocument : BaseDocument
    {
        public int NumberOfCopies { get; set; }

        public override void Print(Stream outputStream)
        {
            for (var x = 0; x < NumberOfCopies; x++)
            {
                base.Print(outputStream);
            }
        }
    }
}