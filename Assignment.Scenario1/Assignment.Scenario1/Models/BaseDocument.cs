﻿using System.IO;
using Assignment.Scenario1.Interfaces;

namespace Assignment.Scenario1.Models
{
    public class BaseDocument : IPrintableDocument
    {
        public string Name { get; set; }
        public string Content { get; set; }

        public virtual void Print(Stream outputStream)
        {
            var sw = new StreamWriter(outputStream);
            sw.Write(Name);
            sw.Write(Content);
            sw.Flush();
        }
    }
}