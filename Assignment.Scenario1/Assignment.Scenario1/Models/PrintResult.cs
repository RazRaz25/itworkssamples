﻿namespace Assignment.Scenario1.Models
{
    public class PrintResult
    {
        public bool WasSuccessful { get; set; }
        public string CompletedTimeStamp { get; set; }
    }
}