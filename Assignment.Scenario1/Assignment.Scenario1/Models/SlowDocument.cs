﻿using System.IO;
using System.Threading;

namespace Assignment.Scenario1.Models
{
    public class SlowDocument : BaseDocument
    {
        public int Delay { get; set; }
        public int NumberOfCopies { get; set; }

        public override void Print(Stream outputStream)
        {
            for (var x = 0; x < NumberOfCopies; x++)
            {
                if (x > 0) Thread.Sleep(Delay*1000);
                base.Print(outputStream);
                
            }
        }
    }
}