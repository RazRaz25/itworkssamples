﻿using System.IO;

namespace Assignment.Scenario1.Interfaces
{
    public interface IPrintableDocument
    {
        void Print(Stream outputStream);
    }
}